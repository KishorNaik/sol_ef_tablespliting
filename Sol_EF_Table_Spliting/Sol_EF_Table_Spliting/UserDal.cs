﻿using Sol_EF_Table_Spliting.EF;
using Sol_EF_Table_Spliting.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Spliting
{
    public class UserDal 
    {
        #region Declaration
        private UserDBEntities db = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            db = new UserDBEntities();
        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<UserEntity>> GetUser()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                        db
                        ?.tblUserAlls
                        ?.AsEnumerable()
                        ?.Select(this.SelectUser)
                        ?.ToList();

                    return getQuery;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tblUserAll,UserEntity> SelectUser
        {
            get
            {
                return
                    (leTblUserObj) => new UserEntity()
                    {
                        FirstName = leTblUserObj.FirstName,
                        LastName = leTblUserObj.LastName,
                        UserId = (int?)leTblUserObj.UserId,
                        UserLogin = new UserLoginEntity()
                        {
                            UserName = leTblUserObj?.Login?.UserName,
                            Password = leTblUserObj?.Login?.Password

                        },
                        UserCommunication = new UserCommunicationEntity()
                        {
                            EmailId = leTblUserObj?.Communication?.EmailId,
                            MobileNo = leTblUserObj?.Communication?.MobileNo
                        }
                    };
            }
        }
        #endregion
    }
}
